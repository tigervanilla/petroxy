var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    router.get('/home', welcomeCtrl.showHomePage);
    router.get('/page', welcomeCtrl.showPagepage);
    router.get('/shift', welcomeCtrl.showShiftpage);
    router.get('/report', welcomeCtrl.showReportpage);
    router.get('/tank', welcomeCtrl.showtankPage);
    router.get('/employee', welcomeCtrl.showEmployeePage);
    router.get('/product', welcomeCtrl.showProductPage);
    router.get('/finance', welcomeCtrl.showfinancePage);
    
    return router.middleware();
}
