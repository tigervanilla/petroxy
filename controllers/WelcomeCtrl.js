var sessionUtils = require('../utils/sessionUtils');

var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');


module.exports = {
    showHomePage: function* (next) {
        var pid=this.request.query.pid;
        //var pid=1;
        //Keeping this query just for sample query
        /*
        var queryString='select * from user where id = %s';
        var query=util.format(queryString,userid);
        var result=yield databaseUtils.executeQuery(query);
        var userDetails=result[0];
        */

        var petrolpumpQueryString='select * from petrolpump where id=%s';
        var petrolpumpQuery=util.format(petrolpumpQueryString,pid);
        var petrolpumpResult=yield databaseUtils.executeQuery(petrolpumpQuery);

        //For total sale of HSD and MS
        var saleQueryString='SELECT N.TYPE,SUM(DOR-DCR-PUMP_TEST-SELF) AS SALE FROM SHIFT S,SHIFT_NOZZLE_DSM SND,NOZZLE N WHERE SND.NOZZLE_ID=N.ID AND SND.SHIFT_ID=S.ID AND DATE(S.START_TIME)=CURDATE() AND S.PID=%s GROUP BY N.TYPE';
        var saleQuery=util.format(saleQueryString,pid);
        var saleResult=yield databaseUtils.executeQuery(saleQuery);
        
        //For toal quantity left of HSD and MS
        var quantityQueryString='SELECT N.TYPE,SUM(DCR) AS QTY_LEFT FROM SHIFT S,SHIFT_NOZZLE_DSM SND,NOZZLE N WHERE SND.NOZZLE_ID=N.ID AND SND.SHIFT_ID=S.ID AND DATE(S.START_TIME)=CURDATE() AND S.PID=%s GROUP BY S.ID,N.TYPE LIMIT 2;';
        var quantityQuery=util.format(quantityQueryString,pid);
        var quantityResult=yield databaseUtils.executeQuery(quantityQuery);

        //For total onhand cash
        var cashQueryString='SELECT TOTAL FROM CASH WHERE PID=%s ORDER BY ID DESC LIMIT 1';
        var cashQuery=util.format(cashQueryString,pid);
        var cashResult=yield databaseUtils.executeQuery(cashQuery);

        //For total bank balance
        var bankQueryString='SELECT SUM(BALANCE) AS TOTAL FROM BANK_ACCOUNT WHERE PID=%s AND ACTIVE=1';
        var bankQuery=util.format(bankQueryString,pid);
        var bankResult=yield databaseUtils.executeQuery(bankQuery);

        //For total fleet balance
        var fleetQueryString='SELECT BALANCE AS TOTAL FROM FLEET WHERE PID=%s ORDER BY ID DESC LIMIT 1';
        var fleetQuery=util.format(fleetQueryString,pid);
        var fleetResult=yield databaseUtils.executeQuery(fleetQuery);

        
        var shiftQueryString='SELECT S.START_TIME,S.END_TIME,U1.FNAME AS SUPERVISOR FROM SHIFT S,USER U1 WHERE S.SUPERVISOR_ID=U1.ID AND S.PID=%s ORDER BY S.ID DESC LIMIT 1;';
        var shiftQuery=util.format(shiftQueryString,pid);
        var shiftResult=yield databaseUtils.executeQuery(shiftQuery);
        
        var nozzleCountQueryString='SELECT COUNT(*) as C FROM NOZZLE WHERE PID=%s';
        var nozzleCountQuery=util.format(nozzleCountQueryString,pid);
        var nozzleCountResult=yield databaseUtils.executeQuery(nozzleCountQuery);
        
        var shiftDetailsQueryString='SELECT U2.FNAME AS DSM,NOZZLE_NUMBER,DOR FROM SHIFT S,NOZZLE N,SHIFT_NOZZLE_DSM SND, USER U2 WHERE SND.NOZZLE_ID=N.ID AND U2.ID=SND.USER_ID AND S.ID=SND.SHIFT_ID AND S.PID=%s ORDER BY S.ID DESC LIMIT %s';
        var shiftDetailsQuery=util.format(shiftDetailsQueryString,pid,nozzleCountResult[0].C);
        var shiftDetailsResult=yield databaseUtils.executeQuery(shiftDetailsQuery);
        
        yield this.render('home',{
            petrolpumpResult:petrolpumpResult[0],
            saleResult:saleResult,
            quantityResult:quantityResult,
            cashResult:cashResult[0],
            bankResult:bankResult[0],
            fleetResult:fleetResult[0],
            shiftResult:shiftResult[0],
            shiftDetailsResult:shiftDetailsResult
        });
 
    },
    showShiftpage: function* (next) {
        var pid=this.request.query.pid;

        //Show all available shifts
        var availableShiftQueryString='SELECT SHIFT_TYPE,TIME(STARTTIME) AS START,TIME(ENDTIME) AS END FROM SHIFT_METADATA WHERE PID=%s';
        var availableShiftQuery=util.format(availableShiftQueryString,pid);
        var availableShiftResult=yield databaseUtils.executeQuery(availableShiftQuery);

        //Show all Available Supervisors
        var availableSupervisorQueryString='SELECT FNAME,LNAME FROM USER U,ROLE R,USER_ROLE UR WHERE U.ID=UR.USER_ID AND R.ID=UR.ROLE_ID AND U.PID=%s AND R.TYPE=\'SUPERVISOR\' AND U.ACTIVE=1';
        var availableSupervisorQuery=util.format(availableSupervisorQueryString,pid);
        var availableSupervisorResult=yield databaseUtils.executeQuery(availableSupervisorQuery);

        //Show all available nozzles
        var availableNozzleQueryString='SELECT NOZZLE_NUMBER FROM NOZZLE WHERE PID=%s AND ACTIVE=1';
        var availableNozzleQuery=util.format(availableNozzleQueryString,pid);
        var availableNozzleResult=yield databaseUtils.executeQuery(availableNozzleQuery);

        //Show all available DSM(s)
        var availableDSMQueryString='SELECT FNAME,LNAME FROM USER U,ROLE R,USER_ROLE UR WHERE U.ID=UR.USER_ID AND R.ID=UR.ROLE_ID AND U.PID=%s AND R.TYPE=\'DSM\' AND U.ACTIVE=1';
        var availableDSMQuery=util.format(availableDSMQueryString,pid);
        var availableDSMResult=yield databaseUtils.executeQuery(availableDSMQuery);

        //To end shift
        var endShiftQueryString='SELECT NOZZLE_NUMBER,N.TYPE,U1.FNAME AS SUPERVISOR,U2.FNAME AS DSM, START_TIME,END_TIME,DOR  FROM SHIFT_NOZZLE_DSM SND,USER U1,USER U2,SHIFT S,NOZZLE N WHERE N.ID=SND.NOZZLE_ID AND SND.SHIFT_ID=S.ID AND U1.ID=S.SUPERVISOR_ID AND U2.ID=SND.USER_ID AND DCR=0 AND S.PID=%s';
        var endShiftQuery=util.format(endShiftQueryString,pid);
        var endShiftResult=yield databaseUtils.executeQuery(endShiftQuery);

        //To View Shift
        var viewShiftQueryString='SELECT SM.SHIFT_TYPE, U1.FNAME AS SUPERVISOR, S.START_TIME,S.END_TIME FROM SHIFT_METADATA SM,SHIFT S,USER U1,SHIFT_NOZZLE_DSM SND WHERE SM.ID=S.SHIFT_META_DATA_ID AND S.SUPERVISOR_ID=U1.ID AND S.ID=SND.SHIFT_ID AND DCR!=0 AND S.PID=%s GROUP BY S.ID;';
        var viewShiftQuery=util.format(viewShiftQueryString,pid);
        var viewShiftResult=yield databaseUtils.executeQuery(viewShiftQuery);


        yield this.render('shift',{
            availableShiftResult:availableShiftResult,
            availableSupervisorResult:availableSupervisorResult,
            availableNozzleResult:availableNozzleResult,
            availableDSMResult:availableDSMResult,
            endShiftResult:endShiftResult,
            viewShiftResult:viewShiftResult
        });
    },
    showReportpage: function* (next) {

        var pid=this.request.query.pid;

        /* QUERY FOR GETTING SHIFT DETAILS*/
        var shiftDetailsQueryString='SELECT SHIFT_ID AS ID,SHIFT_TYPE,DATE(STARTTIME) AS DATE,U1.FNAME AS SUPERVISOR FROM SHIFT_NOZZLE_DSM SND,SHIFT S,USER U1,SHIFT_METADATA SM WHERE SHIFT_META_DATA_ID=SM.ID AND SUPERVISOR_ID=U1.ID AND SND.SHIFT_ID=S.ID AND DCR!=0 AND S.PID=%s ORDER BY SHIFT_ID DESC LIMIT 1';
        var shiftDetailsQuery=util.format(shiftDetailsQueryString,pid);
        var shiftDetailsResult=yield databaseUtils.executeQuery(shiftDetailsQuery);

        //Fetched Shift ID
        var sid=shiftDetailsResult[0].ID;
        //var sid=2;
        /* QUERY FOR ELECTRONIC TOTALIZER */
        var elecTotalizerQueryString='SELECT N.NOZZLE_NUMBER AS NOZZLE, N.TYPE AS PRODUCT,U1.FNAME AS ASSIGNTED_TO,DOR,DCR,PUMP_TEST,(DOR-DCR-PUMP_TEST) AS SALE FROM SHIFT_NOZZLE_DSM SND,NOZZLE N,USER U1 WHERE SND.NOZZLE_ID=N.ID AND SND.USER_ID=U1.ID AND SND.SHIFT_ID=%s AND N.PID=%s';
        var elecTotalizerQuery=util.format(elecTotalizerQueryString,sid,pid);
        var elecTotalizerResult=yield databaseUtils.executeQuery(elecTotalizerQuery);

        var elecTotalizerQueryString1='SELECT N.TYPE AS PRODUCT, SUM(DOR-DCR) AS DISP,SUM(PUMP_TEST) AS PUMPTEST,SUM(DOR-DCR-PUMP_TEST) AS SALE FROM SHIFT_NOZZLE_DSM SND,NOZZLE N,USER U1 WHERE SND.NOZZLE_ID=N.ID AND SND.USER_ID=U1.ID AND SND.SHIFT_ID=%s AND N.PID=%s GROUP BY N.TYPE';
        var elecTotalizerQuery1=util.format(elecTotalizerQueryString1,sid,pid);
        var elecTotalizerResult1=yield databaseUtils.executeQuery(elecTotalizerQuery1);

        /* QUERY FOR MANUAL TOTALIZER */
        var manTotalizerQueryString='SELECT N.NOZZLE_NUMBER AS NOZZLE, N.TYPE AS PRODUCT,U1.FNAME AS ASSIGNTED_TO,AOR,ACR,PUMP_TEST,(AOR-ACR-PUMP_TEST) AS SALE FROM SHIFT_NOZZLE_DSM SND,NOZZLE N,USER U1 WHERE SND.NOZZLE_ID=N.ID AND SND.USER_ID=U1.ID AND SND.SHIFT_ID=%s AND N.PID=%s';
        var manTotalizerQuery=util.format(manTotalizerQueryString,sid,pid);
        var manTotalizerResult=yield databaseUtils.executeQuery(manTotalizerQuery);

        var manTotalizerQueryString1='SELECT N.TYPE AS PRODUCT, SUM(AOR-ACR) AS DISP,SUM(PUMP_TEST) AS PUMPTEST,SUM(AOR-ACR-PUMP_TEST) AS SALE FROM SHIFT_NOZZLE_DSM SND,NOZZLE N,USER U1 WHERE SND.NOZZLE_ID=N.ID AND SND.USER_ID=U1.ID AND SND.SHIFT_ID=%s AND N.PID=%s GROUP BY N.TYPE';
        var manTotalizerQuery1=util.format(manTotalizerQueryString1,sid,pid);
        var manTotalizerResult1=yield databaseUtils.executeQuery(manTotalizerQuery1);

        /* QUERY FOR DIFF  BETWEEN ELEC. AND MANUAL TOTALIZER */
        var emdiffQueryString='SELECT N.TYPE AS PRODUCT,SUM(PUMP_TEST) AS PUMPTEST,(SUM(AOR-ACR-PUMP_TEST)-SUM(DOR-DCR-PUMP_TEST)) AS DIFFERENCE FROM SHIFT_NOZZLE_DSM SND,NOZZLE N,USER U1 WHERE SND.NOZZLE_ID=N.ID AND SND.USER_ID=U1.ID AND SND.SHIFT_ID=%s AND N.PID=%s GROUP BY N.TYPE ORDER BY N.TYPE';
        var emdiffQuery=util.format(emdiffQueryString,sid,pid);
        var emdiffResult=yield databaseUtils.executeQuery(emdiffQuery);

        /* QUERY FOR GETTING VOLUME THROUGH HEIGHT*/
        var tankheightQueryString='SELECT SP.SID, P.NAME,SP.QTY AS QTY,CMV+MMV*(SP.QTY-CAST(SP.QTY AS SIGNED))*10 AS VOLUME FROM SHIFT_PRICE SP,PRODUCT P,DATASHEET D,TANK_METADATA TM WHERE P.ID=SP.PRO_ID AND D.PID=%s AND D.TANK_METADATA_ID=TM.ID AND TM.TYPE=P.NAME AND HEIGHT=CAST(SP.QTY AS UNSIGNED) AND SP.SID IN ( %s , %s ) ORDER BY P.NAME,SP.SID';
        var tankheightQuery=util.format(tankheightQueryString,pid,sid,(sid-1));
        console.log(tankheightQuery);
        var tankheightResult=yield databaseUtils.executeQuery(tankheightQuery);

        /* QUERY FOR PRODUCT PRICE */
        var priceQueryString='SELECT DISTINCT(NAME) AS NAME,PRICE FROM PRODUCT P,NOZZLE N WHERE N.TYPE=P.NAME AND P.PID=%s ORDER BY NAME';
        var priceQuery=util.format(priceQueryString,pid);
        var priceResult=yield databaseUtils.executeQuery(priceQuery);

        /* SS CHECKSHEET QUERY FOR ESTIMATING COLLECTION */
        var estqueryString='SELECT NOZZLE_NUMBER,U.FNAME AS ASSIGNED_TO,N.TYPE, (DOR-DCR-PUMP_TEST-SELF) AS SALE,(DOR-DCR-PUMP_TEST-SELF)*PRICE AS COLLECTION FROM SHIFT S, SHIFT_NOZZLE_DSM SND,NOZZLE N,PRODUCT P,USER U WHERE S.ID=SND.SHIFT_ID AND SND.USER_ID=U.ID AND N.ID=SND.NOZZLE_ID AND N.TYPE=P.NAME AND S.ID=%s AND P.PID=%s ORDER BY NOZZLE_NUMBER';
        var estquery=util.format(estqueryString,sid,pid);
        var estResult=yield databaseUtils.executeQuery(estquery);

        /* CHECKSHEET QUERY FOR ACTUAL COLLECTION (TO SHOW) */
        var actQueryString='SELECT NOZZLE_NUMBER,U.FNAME,CM.MODE,SC.AMOUNT FROM SHIFT S,SHIFT_NOZZLE_DSM SND,NOZZLE N,SHIFT_COLLECTION SC,COLLECTION_MODE CM,USER U WHERE S.ID=SND.SHIFT_ID AND SND.NOZZLE_ID=N.ID AND SND.ID=SC.SHIFT_NOZZLE_DSM_ID AND SC.COLLECTION_MODE_ID=CM.ID AND U.ID=SND.USER_ID AND S.PID=%s AND S.ID=%s ORDER BY NOZZLE_NUMBER';
        var actQuery=util.format(actQueryString,pid,sid);
        var actResult=yield databaseUtils.executeQuery(actQuery);

        /* CHECKSHEET QUERY FOR ACTUAL COLLECTION (TO CALCULATE) */
        var actQueryString1='SELECT NOZZLE_NUMBER,U.FNAME AS ASSIGNED_TO ,SUM(SC.AMOUNT) AS COLLECTION FROM SHIFT S,SHIFT_NOZZLE_DSM SND,NOZZLE N,SHIFT_COLLECTION SC,COLLECTION_MODE CM,USER U WHERE S.ID=SND.SHIFT_ID AND SND.NOZZLE_ID=N.ID AND SND.ID=SC.SHIFT_NOZZLE_DSM_ID AND SC.COLLECTION_MODE_ID=CM.ID AND U.ID=SND.USER_ID AND S.PID=%s AND S.ID=%s GROUP BY NOZZLE_NUMBER ORDER BY NOZZLE_NUMBER;';
        var actQuery1=util.format(actQueryString1,pid,sid);
        var actResult1=yield databaseUtils.executeQuery(actQuery1);


        yield this.render('report',{
            shiftDetailsResult:shiftDetailsResult[0],
            elecTotalizerResult:elecTotalizerResult,
            elecTotalizerResult1:elecTotalizerResult1,
            manTotalizerResult:manTotalizerResult,
            manTotalizerResult1:manTotalizerResult1,
            emdiffResult:emdiffResult,
            tankheightResult:tankheightResult,
            priceResult:priceResult,
            estResult:estResult,
            actResult:actResult,
            actResult1:actResult1

        });
    },
    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    },
    showfinancePage: function* (next) {

        var pid=this.request.query.pid;
        var nozzle_number=this.request.query.no;
        
        //querry to show all details about all shifts
        var AllDetailfinanceQueryString='SELECT U1.FNAME AS SUPERVISOR,U2.FNAME AS ASSIGNED_TO,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME,SND.USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SC.ID,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM SND,USER U1,NOZZLE,SHIFT_COLLECTION SC,COLLECTION_MODE CM, USER U2 WHERE SHIFT.ID=SND.SHIFT_ID AND U1.ID=SHIFT.SUPERVISOR_ID AND SND.NOZZLE_ID=NOZZLE.ID AND SND.ID=SC.SHIFT_NOZZLE_DSM_ID AND SND.USER_ID=U2.ID AND CM.ID=SC.COLLECTION_MODE_ID AND SHIFT.PID=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\'';
        var AllDetailfinanceQuery=util.format(AllDetailfinanceQueryString,pid);
        var AllDetailfinanceResult=yield databaseUtils.executeQuery(AllDetailfinanceQuery);
        var shiftDetails=AllDetailfinanceResult;


       //query to show sales according to ALL nozzle number
       var SalesNozzleQueryString='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND USER.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT.PID=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' GROUP BY NOZZLE_NUMBER,MODE';
       var SalesNozzleQuery=util.format(SalesNozzleQueryString,pid);
       var SalesNozzleResult=yield databaseUtils.executeQuery(SalesNozzleQuery);
       var saleDetails=SalesNozzleResult;

       
       //QUERY TO SHOW SALES ACCORDING TO PARTICULAR NOZZLE NUMBER
        
      var SalesNozzleQueryString1='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND USER.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT.PID=%s AND NOZZLE_NUMBER=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\'';
       var SalesNozzleQuery1=util.format(SalesNozzleQueryString1,pid,nozzle_number);
       var SalesNozzleResult1=yield databaseUtils.executeQuery(SalesNozzleQuery1);
       var saleDetails1=SalesNozzleResult1;
       
       //show details of a specific supervisor

       
       var SalesSupervisorQueryString='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND USER.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT.PID=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' AND USER.FNAME=\'AMAN\' group by nozzle_number,mode';
       var SalesSupervisorQuery=util.format(SalesSupervisorQueryString,pid);
       var SalesSupervisorResult=yield databaseUtils.executeQuery(SalesSupervisorQuery);
       var supervisorDetails=SalesSupervisorResult;
       // SHOW DETAILS OF ALL SUPERVISOR

       var SalesSupervisorQueryString1='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND USER.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT.PID=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' group by user.fname, nozzle_number,mode';
       var SalesSupervisorQuery1=util.format(SalesSupervisorQueryString1,pid);
       var SalesSupervisorResult1=yield databaseUtils.executeQuery(SalesSupervisorQuery1);
       var supervisorDetails1=SalesSupervisorResult1;

       //to show details according to particular dsm
        
       var SalesDSMQueryString='SELECT U1.FNAME AS SUPERVISOR,U2.FNAME AS DSM,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM USER U1,SHIFT,SHIFT_NOZZLE_DSM,USER U2,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND U1.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT_NOZZLE_DSM.USER_ID=U2.ID AND SHIFT.PID=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' AND U2.FNAME=\'SITA\'';
       var SalesDSMQuery=util.format(SalesDSMQueryString,pid);
       var SalesDSMResult=yield databaseUtils.executeQuery(SalesDSMQuery);
        var dsmDetails=SalesDSMResult;
       // TO SHOW DETAILS OF ALL DSM


       var SalesDSMQueryString1='SELECT U1.FNAME AS SUPERVISOR,U2.FNAME AS DSM,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM USER U1,SHIFT,SHIFT_NOZZLE_DSM,USER U2,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND U1.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT_NOZZLE_DSM.USER_ID=U2.ID AND SHIFT.PID=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' GROUP BY NOZZLE_NUMBER,START_TIME,MODE';
       var SalesDSMQuery1=util.format(SalesDSMQueryString1,pid);
       var SalesDSMResult1=yield databaseUtils.executeQuery(SalesDSMQuery1);
        var dsmDetails1=SalesDSMResult1;

      //TO VIEW THE DETAILS SUPERVISORWISE,DSMWISE,MODEWISE
       
      var ViewDetailQueryString='SELECT U1.FNAME AS SUPERVISOR,U2.FNAME AS DSM,SHIFT_NOZZLE_DSM.USER_ID AS USER_ID,SUM(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.ID AS ID,MODE,AMOUNT FROM USER U1,SHIFT,SHIFT_NOZZLE_DSM,USER U2,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.ID=SHIFT_NOZZLE_DSM.SHIFT_ID AND U1.ID=SHIFT.SUPERVISOR_ID AND SHIFT_NOZZLE_DSM.NOZZLE_ID=NOZZLE.ID AND SHIFT_NOZZLE_DSM.ID=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_ID AND COLLECTION_MODE.ID=SHIFT_COLLECTION.COLLECTION_MODE_ID AND SHIFT_NOZZLE_DSM.USER_ID=U2.ID AND SHIFT.PID=%s GROUP BY U1.FNAME,U2.FNAME,MODE';
      var ViewDetailQuery=util.format(ViewDetailQueryString,pid);
      var ViewDetailResult=yield databaseUtils.executeQuery(ViewDetailQuery);
       var supdsmDetails=ViewDetailResult;
      


        yield this.render('finance',{
        shiftDetails:shiftDetails,
        saleDetails:saleDetails,
       saleDetails1:saleDetails1,
        supervisorDetails:supervisorDetails,
      supervisorDetails1:supervisorDetails1,
       dsmDetails:dsmDetails,
       dsmDetails1:dsmDetails1,
      supdsmDetails:supdsmDetails 

        });
    },
    showProductPage: function* (next) {
        var pid=this.request.query.pid;
        var type=this.request.query.type;
        var name=this.request.query.name;
       var productqueryString='SELECT * FROM PRODUCT WHERE PID="%s"';
       var productquery=util.format(productqueryString,pid);
       var result4=yield databaseUtils.executeQuery(productquery);
       var productDetails=result4;
  
  
       var productqueryString1='SELECT NAME,PRICE,QTY AS QUANTITY_LEFT FROM PRODUCT WHERE PID="%s" AND TYPE="%s"';
        var productquery1=util.format(productqueryString1,pid,type);
        var result5=yield databaseUtils.executeQuery(productquery1);
        var productDetails1=result5;
  
  
  
       var productqueryString2='SELECT PRICE,QTY AS QUANTITY_LEFT FROM PRODUCT WHERE PID="%s" AND TYPE="%s" AND NAME="%s"';
        var productquery2=util.format(productqueryString2,pid,type,name);
        var result6=yield databaseUtils.executeQuery(productquery2);
        var productDetails2=result6;
  
  
      yield this.render('product',{
        productDetails:productDetails,
        productDetails1:productDetails1,
        productDetails2:productDetails2
      });
    },
    showEmployeePage: function* (next) {
        var pid=this.request.query.pid;

      //for role of employee supervisor
      var employeeQueryString = 'SELECT USER.FNAME,USER.LNAME FROM USER,USER_ROLE,ROLE WHERE USER.PID=%s AND USER.ID=USER_ROLE.USER_ID AND USER_ROLE.ROLE_ID=ROLE.ID AND ROLE.TYPE="SUPERVISOR"';
      var employeeQuery = util.format(employeeQueryString,pid);
      var employeeResult = yield databaseUtils.executeQuery(employeeQuery);

       //for role of employee admin
       var employeeQueryString1 = 'SELECT USER.FNAME,USER.LNAME FROM USER,USER_ROLE,ROLE WHERE USER.PID=%s AND USER.ID=USER_ROLE.USER_ID AND USER_ROLE.ROLE_ID=ROLE.ID AND ROLE.TYPE="ADMIN"';
       var employeeQuery1 = util.format(employeeQueryString1,pid);
       var employeeResult1 = yield databaseUtils.executeQuery(employeeQuery1);

        //for role of employee dsm
      var employeeQueryString2 = 'SELECT USER.FNAME,USER.LNAME FROM USER,USER_ROLE,ROLE WHERE USER.PID=%s AND USER.ID=USER_ROLE.USER_ID AND USER_ROLE.ROLE_ID=ROLE.ID AND ROLE.TYPE="DSM"';
      var employeeQuery2 = util.format(employeeQueryString2,pid);
      var employeeResult2 = yield databaseUtils.executeQuery(employeeQuery2);
      
        yield this.render('employee',{
            employeeResult:employeeResult,
            employeeResult1:employeeResult1,
            employeeResult2:employeeResult2
        });

    },

    showtankPage: function* (next) {
         
        var pid=this.request.query.pid;

        //query to show details of all tanks
        var DetailtankQueryString='SELECT CAPACITY,TYPE,ACTIVE FROM TANK_METADATA WHERE PID=%s';
        var DetailtankQuery=util.format(DetailtankQueryString,pid);
        var DetailtankResult=yield databaseUtils.executeQuery(DetailtankQuery);

        //show all details of a tank
        var AllDetailtankQueryString='SELECT CAPACITY,TYPE,ACTIVE FROM TANK_METADATA WHERE TYPE=\'HSD\' AND PID=%s;';
        var AllDetailtankQuery=util.format(AllDetailtankQueryString,pid);
        var AllDetailtankResult=yield databaseUtils.executeQuery(AllDetailtankQuery);



        yield this.render('tank',{
            DetailtankResult:DetailtankResult,
            AllDetailtankResult:AllDetailtankResult[0]

        });
    },
    showPagepage: function* (next) {
        yield this.render('page',{
        
        });
    },
    showTestpage: function* (next) {
        console.log('123');
        yield this.render('gla',{

    });
} 
}